# ReasonML Presentation Notes

Intended to be used in conjunction with the presentation as an example of some ReasonML syntax and basic structure.

# Build

```
npm run build
```

# Build + Watch

```
npm run start
```

# Editor

If you use `vscode`, Press `Windows + Shift + B` it will build automatically
