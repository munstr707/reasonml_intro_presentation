/* define expressions just as you would in javascript */
let sum = (num1, num2) => num1 + num2;

/* named args */
let namedSum = (~num1, ~num2) => num1 + num2;

/* note the polymorphism gap :) */
let sumFloat = (flt1, flt2) => flt1 +. flt2;

/* types can be annotated if desired, but its often not required */
let testFloat: float = sumFloat(1.2, 1.3);

let testIntSum = sum(1, 1);

let testNamedSum = namedSum(~num1=1, ~num2=2);

let tupTest: (int, char, string) = (1, 's', "string");

let tupTest2 = (3.5, "math rocks", ("nested tuple start", 's'));

let blockScope = {
  let soapOpera = "telenovela";
  soapOpera ++ " not good";
};

Js.log(blockScope);

print_string(blockScope);