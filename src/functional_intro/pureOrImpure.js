const list = [];
const numberOfDogs = 0;

function add(num1, num2) {
  return num1 + num2;
}
const addExpr = (num1, num2) => num1 + num2;

function addToAmountOfDogs(num) {
  return numberOfDogs + num;
}
const addToAmountOfDogsExpr = num => numberOfDogs + num;

function addToList(eleToAdd) {
  list.push(eleToAdd);
}
const addToListExpr = eleToAdd => {
  list.push(eleToAdd);
};
