/* anonymous function */
x => x + 1;

let increment = x => x + 1;

let sampleList = [1, 2, 3, 4];

/* like any functional language, reasonML has first class and higher order functions */
let incrementedList = List.map(increment, sampleList);

Array.of_list(incrementedList) |> Js.log;