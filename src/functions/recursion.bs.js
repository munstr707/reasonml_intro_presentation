// Generated by BUCKLESCRIPT VERSION 2.2.3, PLEASE EDIT WITH CARE
'use strict';


function fibbonacci(x) {
  if (x < 3) {
    return 1;
  } else {
    return fibbonacci(x - 1 | 0) + fibbonacci(x - 2 | 0) | 0;
  }
}

console.log(fibbonacci(3));

console.log(fibbonacci(5));

exports.fibbonacci = fibbonacci;
/*  Not a pure module */
