/* rec keyword -> mutually or self recursive functions need defined with the rec keyword */
let rec fibbonacci = (x: int) : int =>
  if (x < 3) {
    1;
  } else {
    fibbonacci(x - 1) + fibbonacci(x - 2);
  };

fibbonacci(3) |> Js.log;

fibbonacci(5) |> Js.log;