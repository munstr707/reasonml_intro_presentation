let add = (num1, num2) => num1 + num2;

let times = (num1, num2) => num1 * num2;

let square = num => times(num, num);