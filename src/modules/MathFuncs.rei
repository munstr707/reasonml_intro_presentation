let add: (int, int) => int;

let times: (int, int) => int;

let square: int => int;