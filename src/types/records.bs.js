// Generated by BUCKLESCRIPT VERSION 2.2.3, PLEASE EDIT WITH CARE
'use strict';


var pi = 4.0 * Math.atan(1.0);

function computeArea(shape) {
  if (shape.tag) {
    return pi * Math.pow(shape[1], 2.0);
  } else {
    var point2 = shape[1];
    var width = Math.abs(point2[/* x */0] - shape[0][/* x */0]);
    var height = Math.abs(point2[/* y */1] - point2[/* y */1]);
    return width * height;
  }
}

exports.pi = pi;
exports.computeArea = computeArea;
/* pi Not a pure module */
