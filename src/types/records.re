/* records are similar to a tuple in that it has a fixed size,
   but its fields are accessed by name */
type point = {
  x: float,
  y: float /* optional trailing comma */
};

type shape =
  | Rectangle(point, point)
  | Circle(point, float);

let pi = 4.0 *. atan(1.0);

let computeArea = shape =>
  switch shape {
  | Rectangle(point1, point2) =>
    let width = abs_float(point2.x -. point1.x);
    let height = abs_float(point2.y -. point2.y);
    width *. height;
  | Circle(_, radius) => pi *. radius ** 2.0
  };