/* unit is a value which denote nothing and is the only value of that type */
();

/* unlike null in other languages, it is not an element of any other type. */
/* unit is used occassionally in functions which do not return anything */
let printStr = str => Js.log(str);
/* note how js.log returns unit as it simply prints without returning anything */