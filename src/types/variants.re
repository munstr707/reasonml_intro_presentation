type color =
  | Red
  | Orange
  | Yellow
  | Green
  | Blue
  | Purple;

let invert = colorToInvert =>
  switch colorToInvert {
  | Red => Green
  | Orange => Blue
  | Yellow => Purple
  | Green => Red
  | Blue => Orange
  | Purple => Yellow
  };

let inverseOfRed = invert(Red);

/*
  Here we define a point for cointaining x and y coordinates of a point
  We are also beginning to define datastructures as variant types here,
    which can be problematic in defining things positionally and not with labels
 */
type point =
  | Point(float, float);

/* here we define shapes using the points we created */
type shape =
  | Rectangle(point, point)
  | Circle(point, float);

let pi = 4.0 *. atan(1.0);

let computeArea = shape =>
  switch shape {
  | Rectangle(Point(x1, y1), Point(x2, y2)) =>
    let width = abs_float(x2 -. x1);
    let height = abs_float(y2 -. y1);
    width *. height;
  | Circle(_, radius) => pi *. radius ** 2.0
  };

let bottomLeft = Point(-1.0, -2.0);

let topRight = Point(7.0, 6.0);

let circ = Circle(topRight, 5.0);

let rect = Rectangle(bottomLeft, topRight);

computeArea(circ) |> Js.log;

computeArea(rect) |> Js.log;